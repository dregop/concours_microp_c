#include <msp430g2553.h>
#include "robot.h"
#include "ADC.h"

#define ref_ir 650  //valeur de r�f�rence pour d�tection du mur
#define ir_front 2  //num�ro du port du capteur IR avant

unsigned int val = 0;
unsigned int compteur = 0;
unsigned int detect_ir =0;  //variable qui enregistre la d�tection d'un mur
unsigned int appui=0; 		//detection arret d'urgence

void Init_timer(){
	WDTCTL = WDTPW | WDTHOLD;				//Stop watchdog timer
	BCSCTL1= CALBC1_1MHZ; 					//frequence d�horloge 1MHz
	DCOCTL= CALDCO_1MHZ; 					//
	TA1CTL = 0|TASSEL_2 |MC_1 | ID_0; 		//source SMCLK, mode UP,pr�division par 1
	TA1CCR0 = 1000; 						//1ms
	TA1CTL |= TAIE;							//autorisation des interruptions
	TA1CTL &= ~TAIFG;						//RESET FLAG
}

//fonction permettant de tester le capteur
void Test_Capteur_IR(unsigned int num_capteur){

	unsigned int mesure;

	mesure=Val_Capt(num_capteur);

	//on teste si il y a un obstacle
	while (mesure>ref_ir)
	    {
	    	detect_ir=1;
	    	Arret_robot();
	    	mesure=Val_Capt(num_capteur);
	    }

	//si plus d'obstacle on relance le robot
	if (detect_ir==1 && mesure<ref_ir)
	    	{
	    	detect_ir=0;
	    	Demarrer_robot();
	    	}
}

void main(){

	unsigned int i;
	ADC_init();
	Init_timer();
    Init_robot();
    Init_bouton();
    __enable_interrupt();

    Vitesse_robot(148,144);

    Demarrer_robot();
    Direction(2); //AVANCE

    for (i=0;i<375;i++)
    {
    	Test_Capteur_IR(ir_front);

    	_delay_cycles(10000);
    }

    Arret_robot();

    Tourner_Droite(49);

    Arret_robot();
    Demarrer_robot();
    Direction(2); //AVANCE

    for (i=0;i<280;i++)
    {
    	Test_Capteur_IR(ir_front);
    	_delay_cycles(10000);
    }

    Arret_robot();

    Tourner_Gauche(50);

    Arret_robot();
    Demarrer_robot();
    Direction(2); //AVANCE

    for (i=0;i<735;i++)
    {
    	Test_Capteur_IR(ir_front);
    	_delay_cycles(10000);
    }

    Arret_robot();

}

//gestion de l'arr�t d'urgence
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void){
		Arret_robot();
		_delay_cycles(50000000);
		P1IFG &=~BIT3; 						//RAZ P1FG
}








