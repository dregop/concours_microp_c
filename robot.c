#include <msp430g2553.h>
#include "robot.h"
#include "ADC.h"

void Arret_robot(){
	TA1CCTL1=OUTMOD_0;
	TA1CCTL2=OUTMOD_0;
}

void Demarrer_robot(){
	TA1CCTL1|=OUTMOD_7;
	TA1CCTL2|=OUTMOD_7;
}

void Init_robot(){

	//moteur Gauche sens P2.1 et PWM P2.2       //moteur Droit sens P2.5 et PWM P2.4
	P2DIR = (BIT1 | BIT2 | BIT4 | BIT5);

	//opto moteur Gauche P2.0   et moteur Droit P2.3
	P2DIR &= ~(BIT0 | BIT3);

	//fonction primaire sur TA1.1 et TA1.2  (PWM)
	P2SEL = (BIT2|BIT4);
	P2SEL2 &= ~(BIT2|BIT4);

	TA1CTL = TASSEL_2 | MC_1 | ID_0;
	TA1CCR0=200;

}

void Vitesse_robot(unsigned int mot_G, unsigned int mot_D) {

	TA1CCR1 = mot_G; // determine rapport cyclique moteur Gauche
	TA1CCR2 = mot_D; // determine rapport cyclique moteur Droit
}

void Direction(int sens) {

	switch (sens) {
		case 0: //tourner � gauche
			P2OUT |= BIT1;  // 1: sens horaire
			P2OUT |= BIT5;
			break;

		case 1: //tourner � droite
			P2OUT &= ~BIT1;
			P2OUT &= ~BIT5;
			break;

		case 2: // avancer
			P2OUT &= ~BIT1;
			P2OUT |= BIT5;
			break;

		case 3: //reculer
			P2OUT |= BIT1;
			P2OUT &= ~BIT5;
			break;
	}
}

void Init_bouton() {
	P1DIR &= ~BIT3; //bouton S2 d�finit en entr�e
	P1DIR &= ~BIT1; //entr�e capteur IR_Front

	P1DIR|=(BIT0 | BIT6);

	P1SEL = 0x00;
	P1SEL2 = 0x00;
	P1REN |= BIT3;
	P1OUT |= BIT3;  //pull-up
	P1IE |= BIT3;   //activation du signal d'interruption
	P1IES |= BIT3;  //sur front descendant
	P1IFG &= ~BIT3; //RAZ Flag

	P1OUT|=(BIT0 | BIT6);
}

unsigned int Val_Capt(unsigned int capt){

	ADC_Demarrer_conversion(capt);
	return ADC_Lire_resultat();
}

void Tourner_Gauche(unsigned int ms){

		unsigned int i;

		Demarrer_robot();
	    Direction(0); //GAUCHE

	    for (i=0;i<ms;i++)
	    {
	        _delay_cycles(10000);
	    }

}

void Tourner_Droite(unsigned int ms){

		unsigned int i;

		Demarrer_robot();
	    Direction(1); //DROITE

	    for (i=0;i<ms;i++)
	    {
	        _delay_cycles(10000);
	    }

}
