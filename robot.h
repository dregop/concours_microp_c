#include <msp430g2553.h>

void Init_robot();

void Arret_robot();

void Demarrer_robot();

void Vitesse_robot(unsigned int mot_G, unsigned int mot_D);

void Direction(int sens);

void Init_bouton();

unsigned int Val_Capt(unsigned int capt);

void Tourner_Gauche(unsigned int ms);

void Tourner_Droite(unsigned int ms);
